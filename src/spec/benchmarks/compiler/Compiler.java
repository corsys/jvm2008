/*
 * Copyright (c) 2008 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * Copyright (c) 2006 Sun Microsystems, Inc. All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

package spec.benchmarks.compiler;

//import com.sun.tools.javac.main.Main;
import com.sun.tools.javac.util.Context;
import java.util.Arrays;
import javax.tools.ToolProvider;
import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.SimpleJavaFileObject;
import java.util.ArrayList;
import java.util.List;

public class Compiler {    
  protected String[] args;
  protected List<SimpleJavaFileObject> srcsList;
  Context context;	   
  protected JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
  protected Iterable<? extends JavaFileObject> compilationUnits;
	//Main main = new Main("javac");
	SpecFileManager fileManager;
	public boolean skipVerify;
    
    public Compiler(String[] args, List<SimpleJavaFileObject> srcsList) {
    	this.args = args;
      this.srcsList = srcsList;
      spec.harness.Context.getOut().println("Finished initializing args.");
      /*
      for (String str : srcsList) {
           spec.harness.Context.getOut().println(str);
      }
      */
      spec.harness.Context.getOut().println("Finished initializing Compiler.");
    }    
    
    public void compile(int compiles) {      
    	long checkSum = 0;
        for (int i = compiles - 1; i >=0; i--) {
        	context = new Context();
        	SpecFileManager.preRegister(context, this);
          JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
          //standardFileManager = compiler.getStandardFileManager(null, null, null); 
          //System.out.println(fileManager.getClass().getName());
        	//main = new Main("javac");        	
          //int r = main.compile(args, context);
          boolean retval = compiler.getTask(null, fileManager, null, Arrays.asList(args), null, srcsList).call();
          /*
            if (r != 0) {
                spec.harness.Context.getOut().println("ERROR: compiler exit code: "
                		+ Integer.toString(r));
                break;
            }
          */
          if(!retval) {
            spec.harness.Context.getOut().println("ERROR: compiler exited with failure.");
            break;
          }



          if (skipVerify) {
            return;
          }

          /*
          if (i == 0) {
            checkSum = fileManager.getChecksum();
            spec.harness.Context.getOut().println("Total checksum:" + checkSum);
          } else if (i == compiles - 1) {
            checkSum = fileManager.getChecksum();
          } else {
            if (checkSum != fileManager.getChecksum()) {
              spec.harness.Context.getOut().println("Total checksum on " 
                  + i + " loop (" + fileManager.getChecksum() + ") differs from " 
                  + "total checksum gooten on " 
                  + (compiles - 1) + " loop (" + checkSum + ").");
            }
          }
          */
        }
    }        
}

