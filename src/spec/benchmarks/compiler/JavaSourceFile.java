/*
 * Copyright (c) 2008 Standard Performance Evaluation Corporation (SPEC)
 *               All rights reserved.
 *
 * Copyright (c) 2006 Sun Microsystems, Inc. All rights reserved.
 *
 * This source code is provided as is, without any express or implied warranty.
 */

package spec.benchmarks.compiler;

import javax.tools.SimpleJavaFileObject;
import javax.tools.JavaFileObject;
import java.net.URI;

public class JavaSourceFile extends SimpleJavaFileObject {
   public JavaSourceFile(String path) {
       super(URI.create("file://" + path),
             JavaFileObject.Kind.SOURCE);
   }
}
