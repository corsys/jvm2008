#!/bin/bash

#export JAVA_HOME=/opt/jdk1.9.0
#export ALT_BOOTDIR=/opt/jdk1.9.0
#export ALT_JAVA_HOME=/opt/jdk1.9.0
#export HOTSPOT_BUILD_JOBS=8
#export ARCH_DATA_MODEL=64
export JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.45-28.b13.el6_6.x86_64"
export ALT_BOOTDIR="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.45-28.b13.el6_6.x86_64"
export ALT_JAVA_HOME="/usr/lib/jvm/java-1.8.0-openjdk-1.8.0.45-28.b13.el6_6.x86_64"
export HOTSPOT_BUILD_JOBS=8
export ARCH_DATA_MODEL=64
sudo update-alternatives --config java

./build-specjvm.sh "$@"
#java -cp ant/lib/ant.jar:ant/lib/ant-launcher.jar org.apache.tools.ant.Main -f build.xml $*
